package com.example.tentwenty.tentwentytask.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tentwenty.tentwentytask.R;
import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {
    private ArrayList<String> images;
    private LayoutInflater inflater;
    private Context context;
    public ViewPagerAdapter(Context context, ArrayList<String> images) {
        this.context = context;
        this.images=images;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
    @Override
    public int getCount() {
        return images.size();
    }
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.frame_view, view, false);
        ImageView myImage = (ImageView) myImageLayout
                .findViewById(R.id.image);
        RequestOptions options = new RequestOptions()
                .fitCenter()
                .error(R.mipmap.ic_launcher_round);
            Glide.with(context).load(images.get(position)).apply(options).into(myImage);
            view.addView(myImageLayout, 0);
        return myImageLayout;
    }
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }
}

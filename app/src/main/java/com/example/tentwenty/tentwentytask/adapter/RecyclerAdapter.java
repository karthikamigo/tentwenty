package com.example.tentwenty.tentwentytask.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tentwenty.tentwentytask.R;
import com.example.tentwenty.tentwentytask.pojo.Movies;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private List<Movies> movieList;
    private Context mContext;
    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView name;
        @BindView(R.id.adult)
        TextView adult;
        @BindView(R.id.date)
        TextView release_date;
        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
    public RecyclerAdapter(Context context, List<Movies> list) {
        this.movieList = list;
        this.mContext = context;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);

        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Movies movie_item = movieList.get(position);
        holder.name.setText(movie_item.getName());
        holder.release_date.setText(movie_item.getRelaese_date());
        if(movie_item.getAdult().equalsIgnoreCase("true"))
        {
            holder.adult.setText(mContext.getString(R.string.a_rated));
        }
        else
        {
            holder.adult.setText(mContext.getString(R.string.u_rated));
        }
        if(movie_item.getThumbnail() != null) {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher_round)
                    .error(R.mipmap.ic_launcher_round);
            Glide.with(mContext).load(Uri.parse(movie_item.getThumbnail())).apply(options).into(holder.thumbnail);
        }
    }
    @Override
    public int getItemCount() {
        return movieList.size();
    }
}
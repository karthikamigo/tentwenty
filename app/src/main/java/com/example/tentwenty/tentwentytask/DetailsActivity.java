package com.example.tentwenty.tentwentytask;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.tentwenty.tentwentytask.adapter.ViewPagerAdapter;
import com.example.tentwenty.tentwentytask.pojo.Movies;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class DetailsActivity extends AppCompatActivity {

    @BindView(R.id.details_toolbar_title)
    TextView bar_title;
    @BindView(R.id.movie_title)
    TextView movie_name;
    @BindView(R.id.movie_overview)
    TextView overview;
    @BindView(R.id.rating)
    RatingBar popularity;
    @BindView(R.id.details_toolbar)
    Toolbar detail_bar;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private ArrayList<String> imageArray;
    String id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        setSupportActionBar(detail_bar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        if (getIntent() != null) {
            id = getIntent().getStringExtra("id");
        }
        try {
            JSONObject main_obj = new JSONObject(Util.AssetJSONFile("sample.json", DetailsActivity.this));
            JSONArray movies_array = main_obj.getJSONArray("results");
            for (int i = 0; i < movies_array.length(); i++) {
                JSONObject movie_obj = movies_array.getJSONObject(i);

                if (movie_obj.getString("id").trim().equalsIgnoreCase(id.trim())) {
                    bar_title.setText(movie_obj.getString("title"));
                    movie_name.setText(movie_obj.getString("original_title"));
                    overview.setText(movie_obj.getString("overview"));
                    popularity.setRating(Float.parseFloat(movie_obj.getString("popularity")));
                    JSONArray img_paths = movie_obj.getJSONArray("image_paths");
                    imageArray = new ArrayList<String>();
                    for(int j=0;j<img_paths.length();j++) {
                        imageArray.add(img_paths.getString(j));
                    }
                    initPager();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    private void initPager() {
        if (imageArray.size() != 0) {

            mPager = (ViewPager)findViewById(R.id.pager);
            mPager.setAdapter(new ViewPagerAdapter(DetailsActivity.this, imageArray));
            CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
            indicator.setViewPager(mPager);
/*
            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == imageArray.size()) {
                        currentPage = 0;
                    }
                    mPager.setCurrentItem(currentPage++, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 2500, 2500);*/
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(imageArray.size() != 0)
        {
            imageArray.clear();
        }
    }
}

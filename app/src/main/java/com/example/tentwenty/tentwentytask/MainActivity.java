package com.example.tentwenty.tentwentytask;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.example.tentwenty.tentwentytask.adapter.RecyclerAdapter;
import com.example.tentwenty.tentwentytask.adapter.RecyclerItemDecoration;
import com.example.tentwenty.tentwentytask.adapter.RecyclerTouchListener;
import com.example.tentwenty.tentwentytask.pojo.Movies;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    RecyclerAdapter mAdapter;
    @BindView(R.id.home_toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_list)
    RecyclerView recycler;
    ArrayList<Movies> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
       if(isInternetAllowed())
           initList();
        recycler.addOnItemTouchListener(new RecyclerTouchListener(this, recycler, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Intent intent = new Intent(MainActivity.this,DetailsActivity.class);
                intent.putExtra("id",mList.get(position).getId());
                startActivity(intent);
            }
            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }
    private void initList()
    {
        try {
            JSONObject main_obj = new JSONObject(Util.AssetJSONFile("sample.json",MainActivity.this));
            JSONArray movies_array = main_obj.getJSONArray("results");
            //JSONArray movies_array = new JSONArray("result");
            for (int i = 0 ; i < movies_array.length() ; i++)
            {
                JSONObject movie_obj = movies_array.getJSONObject(i);
                Movies model = new Movies();
                model.setId(movie_obj.getString("id"));
                model.setName(movie_obj.getString("title"));
                model.setOriginal_name(movie_obj.getString("original_title"));
                model.setThumbnail(movie_obj.getString("poster_path"));
                model.setRelaese_date(movie_obj.getString("release_date"));
                model.setAdult(movie_obj.getString("adult"));
                mList.add(model);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(mList.size() != 0) {
            mAdapter = new RecyclerAdapter(MainActivity.this, mList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recycler.setLayoutManager(mLayoutManager);
            recycler.addItemDecoration(new RecyclerItemDecoration(MainActivity.this, LinearLayoutManager.VERTICAL, 16));
            recycler.setAdapter(mAdapter);
        }

    }

    //We are calling this method to check the permission status
    private boolean isInternetAllowed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.INTERNET)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.INTERNET}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            initList();
            return true;
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            initList();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_info) {
            startActivity(new Intent(MainActivity.this,InfoActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onPause() {
        super.onPause();
        if(mList.size() == 0)
        {
            initList();
        }
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        if(mList.size() == 0)
        {
            initList();
        }
    }
}
